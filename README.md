# Data Analytics

| qa | master | 
| :-: | :-: | 
|[![pipeline status qa](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/badges/qa/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/-/commits/qa) |[![pipeline status master](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/-/commits/master)|
|[![coverage report qa](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/badges/qa/coverage.svg)](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/-/commits/qa)|[![coverage report master](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/badges/master/coverage.svg)](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/-/commits/master)|

The project contains a suite of tools (and respective packages) to:
1. extract time series data from CERN databases: InfluxDB, ElasticSearch, Spark Cluster.
2. analyze time series data.

This time series data can come from:
- metrics measured for each hypervisor in the Data Centre.
- derived timeseries from log file analysis.

A central part of this project is the package that will contains a different algorithms to analyze the time series data and raise alarms if needed.

## Getting Started
You can use this module in two environments:
1. in the SWAN notebook
1. in your local machine

### Prerequisites

You need to:
1. be able to authenticate via kerberos for the Spark access
```
kinit username@CERN.CH
```
2. have a Grafana token, to access the InfluxDB and ElasticSearch data.


### Installing - SWAN (https://swan.web.cern.ch/)
1. Make sure you start Swan connecting to the right Spark cluster, that in general is `the General Purpose (Analytix)`
2. Paste this in you notebook:
``` python
# necessary to download the authenticate
import getpass
import os, sys

print("Please enter your kerberos password")
ret = os.system("echo \"%s\" | kinit" % getpass.getpass())

if ret == 0: print("Credentials created successfully")
else:        sys.stderr.write('Error creating credentials, return code: %s\n' % ret)
```
2. Execute the cell and insert your passwork. Now you are logged in.
3. Paste this and execute:
```
! pip install --user git+https://:@gitlab.cern.ch:8443/cloud-infrastructure/data-analytics.git
```
4. Instal the package that you want (e.g):
``` python
from spark_etl import cluster_utils
```

### Installing - Local machine
 1. You can directly login to your shell with the command:
 ```
 kinit username@CERN.CH
 ```
 2. Insert you pasword.
 3. Directly install the package from gitlab
 ```
 ! pip install --user git+https://:@gitlab.cern.ch:8443/cloud-infrastructure/data-analytics.git
 ```

## Possible use cases

![use-case-diagram](documentation/diagrams/use-case-data-analytics.png)


## Example Notebooks

You can refer to the [notebook folder](https://gitlab.cern.ch/cloud-infrastructure/data-analytics/-/tree/master/notebooks) to explore some example of usage.

## Authors

This project is created and mantained by the IT-CM-RPS team.
In particular the main active contributors are: Domenico, Patrycja, Matteo.
