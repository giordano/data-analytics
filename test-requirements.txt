coverage>=4.5.1
doc8>=0.8.0
hacking!=0.13.0,<0.14,>=0.12.0
pep257==0.7.0
flake8-docstrings==0.2.1.post1
mock>=2.0.0
stestr>=2.0.0
bandit!=1.6.0,>=1.1.0 # Apache-2.0
