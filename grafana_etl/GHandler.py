from __future__ import print_function    # compatibility readons
from functools import reduce
from grafana_etl import mylogger
import os
import pandas as pd
from pandasticsearch import Agg
import requests

logger = mylogger.getLogger()


class BaseHandler(object):
    """Base Handler class for grafana data extraction.

    Implementation of this class are based on supported
    backends (e.g. ElasticSearch and InfluxDB).
    This class perform request ot the grafana endpoint,
    after authentication via a secret token

    Configuration params are:
    - url: the path of the API to access datasources
        (e.g. "https://monit-grafana.cern.ch/api/datasources")
    - db: the name of the database your are accessing in InfluxDB
        (e.g. "monit_production_collectd")
    - token: the path of the file of your token
        (e.g. "../secrets/grafana_token.txt")
    - request: a python dictionary that contains information
        about what you want to get.
        Usually you can get this information via the graphical
        interface for queries of Grafana.
    """

    valid_args = []

    def __init__(self, **kwargs):
        self.curves = []
        self.jres = {}
        logger.info('kwargs %s' % kwargs)
        self.args = {}
        for arg in self.valid_args:
            try:
                self.args[arg] = kwargs[arg]
            except KeyError:
                self.args[arg] = None

    def getparam(self, params, key):
        """Load parameter value from a dictionary."""
        if key in params.keys():
            return params[key]
        else:
            return None

    def gettoken(self):
        """Load token for authentication."""
        try:
            token_file = self.args['token']
            if not os.path.isfile(token_file):
                # file path is relative to installation
                token_file = os.path.join(os.path.dirname(__file__),
                                          token_file)
            logger.info('Opening token file ' + token_file)
            with open(token_file, 'r') as f:
                self.token = f.read().splitlines()[0]
        except Exception:
            self.token = None

    def getdata(self):
        """Base method to retrieve data from grafana.

        To be implemented in the Derived class.
        """
        raise NotImplementedError('method not implemented in baseclass')

    def handledata(self):
        """Base method to handle the loaded data.

        If not defined in derived classes,
        set the time series data curves to empty dictionary
        """
        self.curves = {}

    def toDF(self, curves):
        """Convert to Pandas DF.

        Convert the dictionary of timeseries in a Pandas dataframe,
        Having each column corresponding to a timeseries
        """
        pdf_merged = None
        for flag, ts in curves.items():
            # print('reading timeseries with flag ', flag)
            pdf = pd.DataFrame(ts, columns=['epoch', flag])
            pdf['time'] = pd.to_datetime(pdf.epoch, unit='ms')
            pdf.drop('epoch', axis=1, inplace=True)
            pdf.set_index('time', inplace=True)
            if pdf_merged is None:
                pdf_merged = pdf
                # pdf_merged.rename(
                # columns={'logs':'logs_%s'%flag}, inplace=True)
                # print(pdf_merged.head())
            else:
                pdf_merged = pdf_merged.merge(pdf,
                                              left_index=True,
                                              right_index=True,
                                              how='outer',
                                              suffixes=("", "_%s" % flag)
                                              )
                # print(pdf_merged.head())
        if pdf_merged.keys()[0].find('@') > -1:
            pdf_merged.columns = pd.MultiIndex.from_tuples(
                [x.split('@') for x in pdf_merged.keys()], names=['A', 'B'])
        return pdf_merged

    def perform(self):
        """Perform set of actions.

        1. Get data (i.e. send a request).
        2. Handle data and create an internal
        Pandas DataFrame (in the Handler.pdf attribute)
        """
        self.getdata()
        self.handledata()
        self.pdf = self.toDF(self.curves)

    def plot(self, **kwargs):
        """In order to plot the pdf data."""
        return self.pdf.plot(**kwargs)


class ESLogHandler(BaseHandler):
    """ESLogHandler.

    Specialized extractor for logs from ElasticSearch.
    """

    valid_args = ['url', 'token', 'request', 'name']

    def singlerequest(self, query, params):
        request_file = params['file']
        if not os.path.isfile(request_file):
            # file path is relative to installation
            request_file = os.path.join(os.path.dirname(__file__),
                                        request_file)
        logger.info('Opening request file ' + request_file)
        with open(request_file, 'r') as fin:
            encoded_data = fin.read()
            fin.close()

        # parse args and replace templates
        for k, v in params['args'].items():
            encoded_data = encoded_data.replace('__insert_%s__' % k, v)

        headers = {'Authorization': 'Bearer %s' % self.token,
                   'Content-Type': 'application/json'}
        url = self.args['url']

        try:
            res = requests.post(url, headers=headers, data=encoded_data)
            jres = res.json()
            if 'status' in jres.keys() and jres['status'] != 200:
                raise Exception('response != 200')
            self.jres[query] = jres
        except Exception as e:
            logger.error(e)
            logger.error('Response status: %i' % res.status_code)
            logger.error('Response content: %s' % res._content)
            logger.error('encoded_data: %s' % encoded_data)
            logger.error('response: %s' % res)

    def getdata(self):
        """Retrieve data from elasticsearch endpoint."""
        self.gettoken()
        request = self.args['request']

        for query, params in request.items():
            self.singlerequest(query, params)

    def toDF(self, curves):
        """Convert to Pandas DF.

        Convert the dictionary of timeseries
        in a pandas dataframe, having each column
        corresponding to a timeseries

        It uses the module pandasticsearch,
        and does not require the creation of
        intermediary self.curves
        """
        pdf_merged = None

        for query, qres in self.jres.items():
            try:
                pdf = Agg.from_dict(qres['responses'][0]).to_pandas()
                # print(pdf[pdf.isnull()].head())

                metrics = self.getparam(self.args['request'][query], 'metrics')
                tags = self.getparam(self.args['request'][query], 'tags')
                columns = []
                if tags:
                    columns.extend(tags)
                if metrics:
                    columns.extend(metrics)
                logger.info('query name %s' % query)
                logger.info('columns selected %s' % columns)

                pdf.reset_index(inplace=True)
                # print(pdf.head())
                pdf['time'] = pd.to_datetime(pdf.epoch, unit='ms')

                pdf.drop('epoch', axis=1, inplace=True)
                pdf.set_index('time', inplace=True)
                # #print(pdf[pdf.index.isnull()].head())
                pdf = pdf[columns]

                pdf = pdf.set_index(tags, append=True).unstack(tags)
                # ##print(pdf[pdf.index.isnull()].head())

                # decided to avoid multi-index to simplify stack/unstack
                # pdf.columns = pd.MultiIndex.from_tuples([(query, x)
                # for x in pdf.keys()], names=['Q', 'M'])

                # using a query@variable column name
                # pdf.columns = ['%s@%s'%(query, x) for x in pdf.keys()]
                # pdf.set_index(, append=True).unstack(level=1).head()

                pdf.columns = ['_'.join(reversed(col)
                                        ).strip()
                               for col in pdf.columns.values]

                if pdf_merged is None:
                    pdf_merged = pdf
                else:
                    pdf_merged = pdf_merged.merge(pdf,
                                                  left_index=True,
                                                  right_index=True,
                                                  how='outer',
                                                  suffixes=("", "_b"))
            except Exception as e:
                logger.error('for query %s Error %s' % (query, e))

        pdf = pdf_merged
        pdf = pdf.tz_localize('UTC').tz_convert('Europe/Paris')
        pdf.fillna(value=0, inplace=True)
        pdf = pdf.loc[pd.notnull(pdf.index)]
        return pdf


class InfluxDBHandler(BaseHandler):
    """InfluxDBHandler.

    Specialized extractor for timeseries from InfluxDB.
    """

    valid_args = ['url', 'token', 'request', 'db']

    def getdata(self):
        """Retrieve data from InfluxDB endpoint.

        The output data is a json document, as retrieved from Grafana
        and formatted for Grafana dashboards.
        Output data is stored in self.jres[query]
        """
        self.gettoken()
        url = self.args['url']
        print(self.args)
        request = self.args['request']
        print(request)
        headers = {'Authorization': 'Bearer %s' % self.token}
        print(request.items())
        for query, params in request.items():
            try:
                logger.info('query name %s' % query)
                req_url = '%s/proxy/%d/query' % (url, params['id'])
                params['db'] = self.args['db']
                req_params = {}
                for k, v in params.items():
                    if k != 'id':
                        req_params[k] = v
                res = requests.get(req_url, headers=headers, params=req_params)
                self.jres[query] = res.json()
            except Exception as e:
                logger.error(e)
                logger.error('req_params: %s' % req_params)
                logger.error('headers: %s' % headers)

    def handledata(self):
        """Manipulate json results.

        Extract timeseries from self.jres,
        reorganise them and load in self.curves
        timeseries data member.
        """
        def reduce_tags(tags, query=''):
            return reduce(lambda x, y: '%s#%s' % (x, y),
                          tags.values(), query + '@')

        curves = {}
        try:
            # print to see what is self.jres.items()
            for query, res in self.jres.items():
                for x in res['results'][0]['series']:
                    curves[reduce_tags(x['tags'], query)] = x['values']
            self.curves = curves
        except Exception as e:
            logger.error(e)
