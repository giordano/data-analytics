from grafana_etl import mylogger
import yaml

logger = mylogger.getLogger(mylogger.logging.INFO)


class ETLDriver(object):
    """Driver.

    Manage the creation of multiple ETL Objects
    via yml file
    """

    def __init__(self, **kwargs):
        valid_args = ['config_name']
        for arg in valid_args:
            try:
                self.__dict__[arg] = kwargs[arg]
            except KeyError:
                self.__dict__[arg] = None
        self.config()

    def config(self):
        """Configure Driver.

        Load configuration from yml file
        and run the ETL creation
        """
        filename = "etc/ETLDriver.yml"
        if self.__dict__["config_name"]:
            filename = self.__dict__["config_name"]

        self.cfgs = yaml.safe_load(open(filename))
        self.confETL()

    def confETL(self):
        """Configure ETLs.

        Create and configure ETLs following the
        definition in the configuration file
        """
        self.etl = {}
        for section, opts in self.cfgs.items():
            logger.info('Configuring ETL %s\n' % section)
            # print 'ETL options', opts
            'Skip sections that start with .'
            if section[0] == '.':
                logger.info('skip this ETL because commented')
                continue
            try:
                self.etl[section] = ETL(section, opts)
            except Exception as e:
                logger.error(e)

    def perform(self):
        """Perform actions.

        Loop on the ETLs and perform actions (query and data analysis)
        as defined in the config file
        """
        for name, etl in self.etl.items():
            logger.info('Running ETL %s' % name)
            etl.perform()


class ETL(object):
    """ETL Class.

    Handle the Extration Transform and Load
    of data from grafana to Pandas dataframe.
    Two data backend can be queried:
    InfluxDB and ElasticSearch)
    A Handler class per ETL allow to extract
    multiple time-series data and load them
    in the same pandas dataframe
    Base Analyser class and some implementations
    are also available in order to execute
    time-series analysis
    """

    handler = None
    opts = None

    def __init__(self, name, opts):
        self.name = name
        self.opts = opts
        self.analysers = []

        # set Handler
        self.setClass(self.opts, 'handler')

        # set Analysers
        if 'analysers' in self.opts.keys():
            for analyserName in self.opts['analysers'].keys():
                if self.setClass(self.opts['analysers'], analyserName):
                    self.analysers.append(analyserName)
            logger.info('registered analysers are %s' % self.analysers)

    def setClass(self, opts, var_name):
        """setClass.

        Following the yaml definition, create dynamically a class
        Eg.:
        if in the yml configuration an entry such as
        handler:
            class: GHandler.ESLogHandler
        is introduced, an object of kind GHandler.ESLogHandler is generated
        This uses the python setattr and getattr functions
        """
        if var_name not in opts.keys():
            logger.error('Error, no %s section in provided options' % var_name)
            setattr(self, var_name, None)
            return False
        else:
            try:
                setattr(self, var_name, self.getObject(opts[var_name]))
                return True
            except Exception as e:
                logger.error(e)
                setattr(self, var_name, None)
                return False

    def getObject(self, opts):
        """getObjects.

        Instantiate an Object of a given class,
        passing the module name and class name
        Eg: class: GHandler.ESLogHandler -> GHandler
        is module name; ESLogHandler is class name
        """
        try:
            module, class_name = opts['class'].rsplit('.', 1)[0:2]
            logger.info(
                "Request for module %s and class_name %s"
                % (module, class_name)
            )
            m = __import__(module)
            if hasattr(m, class_name):
                objectClass = getattr(m, class_name)
                return objectClass(**opts)
            else:
                print("no class %s in module %s" % (class_name, module))
                return None
        except Exception as e:
            logger.error(e)
            return None

    def perform(self):
        """Perform actions in the ETL object.

        First action is to run the data handler
        that extracts the data and convert them
        in a pandas DF.
        Second action is to run a number of
        independent analysers
        """
        if self.handler:
            self.handler.perform()
            if 'analysers' in self.opts.keys():
                for analyserName in self.analysers:
                    analyser = getattr(self, analyserName)
                    logger.info("I have an analyser: %s" % analyserName)
                    analyser.setData(self.handler.pdf)
                    logger.debug(analyser.pdfs['input'].keys())
                    analyser.runStrategy()
