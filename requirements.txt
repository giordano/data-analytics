setuptools
pbr!=2.1.0,>=2.0.0
matplotlib
numpy
pandas
pandasticsearch
requests
scipy
six
pyyml

