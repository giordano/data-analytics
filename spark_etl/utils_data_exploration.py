"""Utility functions for Data Exploration.

AGE + NUMEROSITY + COLUMNS OF COLLECTED PLUGIN DATA

You have functions to explore your data and create DataFrames
to answer questions such as:
- how many hypervisors have a specific collectd plugin installed?
- when was that plugin installed in those hypervisors?
- which metrics are collected by a plugin? (i.e. columns exploration)

NB. All questions are restircted to the data belonging to the chosen
MACRO_HOSTGROUP (e.g. 'cloud_compute/level2/')

"""


from datetime import datetime

import json
import pandas as pd
import matplotlib.pyplot as plt
from pyspark.sql.utils import AnalysisException

from spark_etl.cluster_utils import get_elements_in_dir
from spark_etl.cluster_utils import MACRO_HOSTGROUP
from spark_etl.cluster_utils import scan_plugin_folder
from spark_etl.cluster_utils import TIMESTAMP_TO_CONSIDER


# DATA EXPLORATION - HOW OLD MY DATA ARE / HOW MANY MACHINES -----------------


def find_oldest_date(spark, plugin_name, full_check=False,
                     earliest_year_possible=2010):
    """Find oldest data collected by this plugin.

    Look inside the folder at the following path:
    /project/monitoring/collectd/<plugin_name>
    Open all parquet files and find the oldest data
    (aka the minimum timestamp).
    Full check = true: loads all the data at once and check
    the earliest date
    Full check = false: loads the data smartly. Starting
    from the oldest data month by month, and looking for
    machines in MACRO_HOSTGROUP, if no machine is there,
    a more recent month is checked and so on.
    earliest_year_possible is used only with full_check =
    false to avoid data that are too old.
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    global TIMESTAMP_TO_CONSIDER  # pylint: disable=global-statement
    # columns = filtered_df.schema.names
    # print("This parquet files has the following schema \n %s" % columns)
    if full_check:
        # not recommended - folders_to_scan is not supported
        folders_to_scan = []
        in_path = "/project/monitoring/collectd/" + plugin_name + "/*/*/*"
        full_df = spark.read.parquet(in_path)
        # oldest date
        filtered_df = full_df.filter(
            (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))
        min_timestamp = filtered_df.agg({TIMESTAMP_TO_CONSIDER: "min"}) \
                                   .collect()[0][0]
    else:
        min_timestamp = None
        folders_to_scan = scan_plugin_folder(plugin_name)
        # remove folders that are too old
        folders_to_scan = [x for x in folders_to_scan
                           if int(x.split("/")[-2]) > earliest_year_possible]
        print("to scan: %s" % folders_to_scan)
        while (min_timestamp is None) and (len(folders_to_scan) > 0):
            # check in the oldest folder
            in_path = folders_to_scan[0]
            print("Checking: %s" % in_path)
            full_df = spark.read.parquet(in_path + "/*")
            # oldest date
            filtered_df = full_df.filter(
                (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))
            min_timestamp = filtered_df.agg({TIMESTAMP_TO_CONSIDER: "min"}) \
                                       .collect()[0][0]
            if (min_timestamp is None):
                # pop remove the checked
                folders_to_scan.pop(0)

    return (min_timestamp, folders_to_scan)


def find_nr_of_hostgroups(spark, plugin_name, year="2020", month="03"):
    """Find nr hostgroups that have this plugin installed.

    Look inside the folder at the following path:
    /project/monitoring/collectd/<plugin_name>
    Open all parquet files and find the nr of
    hostgroups that are saved in those data.
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    in_path = ("/project/monitoring/collectd/" + plugin_name +
               "/" + year + "/" + month + "/*")
    try:
        full_df = spark.read.parquet(in_path)
    except AnalysisException:
        return 0, []
    filtered_df = full_df.filter(
        (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))

    # nr hostgroup
    projected_df = filtered_df.select('submitter_hostgroup').distinct()
    hostgroups_presents = projected_df.collect()
    hostgroups_presents = [x[0] for x in hostgroups_presents]
    nr_hostgroups = len(hostgroups_presents)
    return nr_hostgroups, hostgroups_presents


def analyze_all_plugins(spark, swan_outfolder):
    """Summarize plugins characteristics in json files.

    Loop over all the plugins in the collectd folder
    and for each compute (always referring only to
    machines of cloud compute level 2):
    - oldest timestamp
    - list of months
    - nr of hostrgroups
    - list of hostgroups
    """
    all_folders = get_elements_in_dir("/project/monitoring/collectd",
                                      return_absolute_path=False)
    plugin_list = [x for x in all_folders if (x[0] != ".")]
    # print(plugin_list)
    # return
    # all_folders
    for plugin in plugin_list[19:]:
        print("----------------------------")
        print("JOBS FOR %s" % plugin)
        # check oldest date
        min_timestamp, folders_after = find_oldest_date(spark, plugin)
        time_data = dict()
        time_data['oldest_timestamp'] = min_timestamp
        time_data['folders_availables'] = folders_after
        if min_timestamp is None:
            time_data['str_oldest_timestamp'] = "None"
        else:
            ts_in_ms = min_timestamp / 1000
            dt_object = datetime.fromtimestamp(ts_in_ms)
            time_data['str_oldest_timestamp'] = str(dt_object)
        # save findings
        with open(swan_outfolder + "/" +
                  plugin + '_months.json', 'w') as outfile:
            json.dump(time_data, outfile)

        # check nr hostgroups
        nr_hostgroups, hostgroups_presents = \
            find_nr_of_hostgroups(spark, plugin)
        hostgroup_data = dict()
        hostgroup_data['nr_hostgroups'] = nr_hostgroups
        hostgroup_data['hostgroups_presents'] = hostgroups_presents
        # save findings
        with open(swan_outfolder + "/" +
                  plugin + '_hostgroups.json', 'w') as outfile:
            json.dump(hostgroup_data, outfile)


def costruct_pdf_statisitc_table(swan_outfolder="statistics-about-plugins"):
    """Create a pandas dataframe to visualize the json just created.

    ASSUMPTION: you have executed analyze_all_plugins() before
    Given a folder containing json results from the analysis
    it creates a joint table.
    """
    all_folders = get_elements_in_dir("/project/monitoring/collectd",
                                      return_absolute_path=False)
    plugin_list = [x for x in all_folders if x[0] != "."]
    df = pd.DataFrame()
    # print(df.head())
    for plugin in plugin_list:
        print("----------------------------")
        print("READING %s" % plugin)
        try:
            with open(swan_outfolder + "/" +
                      plugin + '_months.json') as json_file:
                data_time = json.load(json_file)
                # print(data)
            with open(swan_outfolder + "/" +
                      plugin + '_hostgroups.json') as json_file:
                data_nr = json.load(json_file)
                # print(data)
            if (data_time["oldest_timestamp"] is not None):
                just_names = [x.split("/")[-2:]
                              for x in data_nr['hostgroups_presents']]
                df_row = pd.DataFrame({
                    "name": [plugin],
                    "timestamp": [data_time['oldest_timestamp']],
                    "date": [data_time['str_oldest_timestamp']],
                    "nr_hostgroups_in_March_month": [data_nr['nr_hostgroups']],
                    "list_hostgroups_batch": [sorted([x for x in just_names
                                                      if x[0] == "batch"])],
                    "list_hostgroups_main": [sorted([x for x in just_names
                                                     if x[0] == "main"])],
                    "list_hostgroups_other": [sorted([x for x in just_names
                                                      if (not (x[0] == "main") and  # noqa : E501
                                                          not (x[0] == "batch"))])]  # noqa : E501
                    })
                df = df.append(df_row)
                print("OK")
            else:
                print("Empty files")
        except FileNotFoundError:
            print("File not found")
    return df


# DATA EXPLORATION - WHICH METRICS / COLUMNS ---------------------------------


def get_metrics_of_plugin(spark, plugin_name="cpu", year="2020", month="03"):
    """Create a json file with the attributes and type_instance of the plugin.

    Get the metrics/attributes that are collected by a
    single collectd plugin. Usually type instance is a good
    indicator for that.
    It returns the list of the metrics.
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    result_dict = {}
    # naming conventions discussion: python -> snake_case
    # https://stackoverflow.com/questions/5543490/json-naming-convention
    result_dict["metrics_count"] = 0
    result_dict["metrics_names"] = []
    result_dict["columns"] = []

    # read and filter
    in_path = ("/project/monitoring/collectd/" + plugin_name +
               "/" + year + "/" + month + "/*")
    try:
        full_df = spark.read.parquet(in_path)
        result_dict["columns"] = full_df.schema.names
    except AnalysisException:
        print("Data found - plugin %s doesn't have the data" /
              " for this period: %s/%s" % (plugin_name, year, month))
        return result_dict
    filtered_df = full_df.filter(
        (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))

    # collect metrics
    projected_df = filtered_df.select('type_instance').distinct()
    metrics_presents = projected_df.collect()
    result_dict["metrics_names"] = [x[0] for x in metrics_presents]
    result_dict["metrics_count"] = len(metrics_presents)
    return result_dict


def collect_metrics_all_plugins(spark,
                                swan_outfolder="statistics-about-plugins",
                                out_file_name="all_metrics",
                                override=False, year="2020", month="03"):
    """Collect the metrics for every plugin.

    It creates a file json containing the metrics of all the plugins.
    Practically, it loops over all the plugins in the collectd folder
    and for each we get the metrics (always referring only to machines
    of cloud compute level 2).
    NB: we look only at the specified year and month data to speed up
    the process. It is assumed that older data contains the same info.
    If override = False it appens all the result in a file.
    """
    all_folders = get_elements_in_dir("/project/monitoring/collectd",
                                      return_absolute_path=False)
    plugin_list = [x for x in all_folders if (x[0] != ".")]
    print(plugin_list)
    if override:
        with open(swan_outfolder + "/" + out_file_name + ".json", 'w') as f:
            json.dump({}, f)
    # return all_folders
    for plugin_name in plugin_list[:]:
        print("----------------------------")
        print("JOBS FOR %s" % plugin_name)
        plugin_dict = {}
        plugin_dict[plugin_name] = get_metrics_of_plugin(spark, plugin_name,
                                                         year, month)
        # check if previous file is there: read it
        try:
            with open(swan_outfolder + "/" + out_file_name + ".json") as f:
                previous_data = json.load(f)
        # otherwise: create an empty dictionary and dump that afterwards
        except FileNotFoundError:
            previous_data = {}
        previous_data.update(plugin_dict)
        new_data = previous_data
        with open(swan_outfolder + "/" + out_file_name + ".json", 'w') as f:
            json.dump(new_data, f)


def inspect_collected_metrics(swan_folder="statistics-about-plugins",
                              file_name="all_metrics"):
    """Visualize the collected metrics all together.

    ASSUMPTION: you have executed collect_metrics_all_plugins() before
    Given a folder containing json results from the analysis
    it prints the json content of the analysis
    """
    with open(swan_folder + "/" + file_name + ".json") as f:
        data = json.load(f)
    print(json.dumps(data, indent=4, sort_keys=True))


# PLOTTING -------------------------------------------------------------------


def plot_each_plugin(local_file_path, **pltargs):
    """Plot the timeseries in the csv, one plot per plugin.

    The plotting function is the one of Pandas, pass kwy argument that can be
    interpreted by its api DataFrame.plot().
    """
    pdf_all_data = pd.read_csv(local_file_path)
    for plugin_name in list(pdf_all_data.plugin.unique()):
        pdf_plugin = pdf_all_data[pdf_all_data["plugin"] == plugin_name]
        pdf_swarm = pdf_plugin.pivot(columns="host",
                                     index="temporal_index",
                                     values="mean_value")
        ax = pdf_swarm.plot(figsize=(22, 10), **pltargs)
        ax.set_title(plugin_name)
        plt.show()
