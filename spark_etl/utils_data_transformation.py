
"""Utility functions for Data Transformation in Pyspark.

Temporal Granularity + Tumbling Window Statistics

DEF: When a windowed query processes each window in a non-overlapping manner,
the window is referred to as a tumbling window. In this case, each record on
an in-application stream belongs to a specific window. It is processed only
once (when the query processes the window to which the record belongs).
Source: https://docs.aws.amazon.com/kinesisanalytics/latest/dev/tumbling-window-concepts.html

It creates new dataframe parquet files that contain an aggregation
in terms of time (nr of minutes), plus some statistics for each group
of data (e.g. mean, min, max, pencentile (10, 25, 75, 90))

"""  # noqa : E501


from collections import defaultdict
import subprocess

import numpy as np
import pyspark.sql.functions as F  # noqa : F401
from pyspark.sql.types import FloatType

from spark_etl.cluster_utils import get_date
from spark_etl.cluster_utils import get_elements_in_dir
from spark_etl.cluster_utils import write_spark_df
from spark_etl.cluster_utils import MACRO_HOSTGROUP


# DATA TRANSFORMATION - CREATE AGGREGATES ------------------------------------


def create_aggregate_specific_path(spark, inpath, outpath,
                                   n_minutes, selected_cell,
                                   attributes,
                                   timestamp_column='event_timestamp',
                                   file_per_day=1):
    """It reads a parquet file at the inpath location.

    It implicitely considers only data of "coud_compute/level2/" in the
    'submitter_hostgroup' column.
    For every 'host' and 'type_instance', this function aggregates the data
    every n_minutes (using column 'event_timestamp'),
    by creating statistics regarding the column 'value' (that usually represent
    the main quantity collected by the collectd plugin). It computes:
    - mean
    - min, max
    - percentile (10, 25, 75, 90) (column name 'perc_10')
    Finally the complete database is saved in the location "outpath"
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    # load data
    full_df = spark.read.parquet(inpath)

    # filter data by MACRO GROUP
    filtered_df = full_df.filter(
        (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))
    # filter only one cell
    if len(selected_cell) > 0:
        filtered_df = filtered_df.filter(
            (filtered_df.submitter_hostgroup.contains(selected_cell[0])))
        # TODO(Domenico) enable to have a list of possible cells  #noqa
    # filter all the attributes - a stands for attribute
    (a_plugin_instance, a_type, a_type_instance, a_value_instance) = attributes
    # filter plugin instance
    if a_plugin_instance is not None:
        filtered_df = filtered_df.filter(
            (filtered_df.plugin_instance == a_plugin_instance))
    if a_type is not None:
        filtered_df = filtered_df.filter(
            (filtered_df.type == a_type))
    if a_type_instance is not None:
        filtered_df = filtered_df.filter(
            (filtered_df.type_instance == a_type_instance))
    if a_value_instance is not None:
        filtered_df = filtered_df.filter(
            (filtered_df.value_instance == a_value_instance))
    filtered_df.show(3)
    # keep only interesting column
    df_projection = filtered_df \
        .withColumn(
            'n_min_group',
            (filtered_df[timestamp_column] / 1000) -
            ((filtered_df[timestamp_column] / 1000) % (60 * n_minutes))
        ).select(
            timestamp_column,
            'submitter_hostgroup',
            'host',
            'plugin',
            'type_instance',
            'value',
            'n_min_group'
        )

    percentile_10_udf = F.udf(lambda y: float(np.percentile(y, 10)),
                              FloatType())
    percentile_25_udf = F.udf(lambda y: float(np.percentile(y, 25)),
                              FloatType())
    percentile_75_udf = F.udf(lambda y: float(np.percentile(y, 75)),
                              FloatType())
    percentile_90_udf = F.udf(lambda y: float(np.percentile(y, 90)),
                              FloatType())

    # create aggregate columns
    df_projection.cache()
    statistics_df = df_projection\
        .groupBy([
            'submitter_hostgroup',
            'host',
            'plugin',
            'type_instance',
            'n_min_group'
        ]).agg(
            F.mean('value').alias('mean_value'),
            F.max('value').alias('max_value'),
            F.min('value').alias('min_value'),
            F.min(timestamp_column).alias(timestamp_column),
            percentile_10_udf(F.collect_list('value')).alias('perc_10'),
            percentile_25_udf(F.collect_list('value')).alias('perc_25'),
            percentile_75_udf(F.collect_list('value')).alias('perc_75'),
            percentile_90_udf(F.collect_list('value')).alias('perc_90')
        ) \
        .orderBy(timestamp_column)

    timestamp_readded_df = statistics_df \
        .withColumn(
            timestamp_column,
            F.from_unixtime((statistics_df[timestamp_column] / 1000),
                            'YYYY-MM-dd HH:mm')
        )
    # https://changhsinlee.com/pyspark-udf/
    # https://danvatterott.com/blog/2018/09/06/python-aggregate-udfs-in-pyspark/

    output_df = timestamp_readded_df
    write_spark_df(output_df, out_path=outpath, num_files=file_per_day)
    return output_df


def create_aggregates_for_each_date(spark, dir_list, out_dir_list, outbasepath,
                                    outfile, n_minutes,
                                    attributes,
                                    selected_cell=[],
                                    timestamp_column='event_timestamp',
                                    file_per_day=1):
    """Create aggregate statistics for each data in the given folder.

    Loop on all the directory in dir_list,
    infer the dates from the directory path.
    Than creates aggregate statistics for each folder
    """
    date_list = defaultdict(list)
    for x in dir_list:
        date_list[get_date(x)] = x

    # loop on dates
    for adate, somedirs in date_list.items():
        print("---------- checking date ", adate)
        out_path = "%s/%s_%s" % (outbasepath, adate.replace("/", "_"), outfile)
        # the tmp data go in another dir
        out_path_tmp = "%s/tmp_%s_%s" % (outbasepath, adate.replace("/", "_"),
                                         outfile)

        # removing and refreshing the tmp
        # given that the tmp is going to evolve, running on tmp in any case
        if out_path_tmp in out_dir_list:
            print("\tremoving %s " % out_path_tmp)
            # !hdfs dfs -rm -r -skipTrash {outpathtmp}
            cmd = 'hdfs dfs -rm -r -skipTrash {}'.format(out_path_tmp)
            subprocess.check_output(cmd, shell=True)

        is_tmp = False
        if somedirs[0].find(".tmp") > 0:
            out_path = out_path_tmp
            is_tmp = True

        if out_path not in out_dir_list or is_tmp:
            print("|---------- the output dir is ", out_path)
            print("|---------- the input dirs are %s" % somedirs)
            try:
                # get_spark_df(somedirs,outpath,nschema)
                create_aggregate_specific_path(spark, somedirs, out_path,   # noqa
                                               n_minutes, selected_cell,
                                               timestamp_column=timestamp_column,  # noqa
                                               file_per_day=file_per_day,
                                               attributes=attributes)
            except Exception as ex:
                print("problem?!")
                print(ex)


def create_aggregates_for_plugins(spark, out_base_path="collectd_result",
                                  plugins=[], n_minutes=15,
                                  selected_cell=[],
                                  year="2020", month="02", day="14",
                                  file_per_day=1):
    """Create statistics for every mentioned plugins.

    plugins: it is a list of tuple that is explaining what you want to
    aggregate with the following pattern:
    (plugin_name, plugin_instance, type, type_instance, value_instance)
    all the elements should be string.
        - plugin_name: determines the table you will look at
        - all the others: determine with which value to filter each column
          before the aggregation process in bin of n minutes (if you want
          to retain every line regardless of those column content you have
          to put None)
    NB you always have to specify a tuple with all the element for each
    plugin.
    """
    # perform checks on the params
    if len(plugins) == 0:
        raise Exception("Empty plugin list")
    if not all([len(x) == 5 for x in plugins]):
        raise Exception("Some plugin doesn't have 5 params (read the doc \
                         string)")
    # output dir in user HDFS area
    # out_base_path="collectd_result"
    for p, *attributes in plugins:
        print(p)
        print("(plugin_instance, type, type_instance, value_instance) = ", attributes)  # noqa
        # output base file name
        out_base_file = "collectd_" + p + ".parquet"
        # input file path with data to process with spark
        in_base_path = ("/project/monitoring/collectd/" + p +
                        "/" + year + "/" + month + "/" + day)
        dir_list = get_elements_in_dir(in_base_path, do_not_modify_path=True)
        out_dir_list = get_elements_in_dir(out_base_path)
        create_aggregates_for_each_date(spark, dir_list, out_dir_list,
                                        out_base_path, out_base_file,
                                        n_minutes, selected_cell=selected_cell,
                                        file_per_day=file_per_day,
                                        attributes=attributes)
