"""Utility functions for PySpark usage.

SPARK + HDFS + JSON

You have functions:
- to setup your spark variables needed to access the cluster.
- to get information about the HDFS folder structure (especially
  in terms of the nested folders that contains data about months
  and days)
- to handle JSON files inside your cluster


"""


from collections import defaultdict
import functools
import json
import os
import random
import re
import subprocess

import pandas as pd
from pyspark import SparkConf  # noqa: F401
from pyspark import SparkContext
from pyspark.sql import SparkSession

from pyspark.sql.types import StructType

MACRO_HOSTGROUP = 'cloud_compute/level2/'
TIMESTAMP_TO_CONSIDER = 'event_timestamp'


# SPARK MANAGEMENT -----------------------------------------------------------


def set_spark(spark_conf=SparkConf()):
    """Set Spark.

    NB: kerberos file in /tmp is expected
    """
    if "SPARK_PORTS" in os.environ:
        ports = os.getenv('SPARK_PORTS').split(',')
    else:
        ports = [random.randrange(5001, 5300),
                 random.randrange(5001, 5300),
                 random.randrange(5001, 5300)]

    spark_conf.set('spark.master', 'yarn')
    spark_conf.set('spark.authenticate', True)
    spark_conf.set('spark.network.crypto.enabled', True)
    spark_conf.set('spark.authenticate.enableSaslEncryption', True)
    spark_conf.set("spark.logConf", True)

    spark_conf.set('spark.driver.host',
                   os.environ.get('HOSTNAME', 'localhost'))
    spark_conf.set('spark.driver.port', ports[0])
    spark_conf.set('spark.blockManager.port', ports[1])
    spark_conf.set('spark.ui.port', ports[2])

    spark_conf.set('spark.executorEnv.PYTHONPATH',
                   os.environ.get('PYTHONPATH'))
    spark_conf.set('spark.executorEnv.LD_LIBRARY_PATH',
                   os.environ.get('LD_LIBRARY_PATH'))
    spark_conf.set('spark.executorEnv.JAVA_HOME',
                   os.environ.get('JAVA_HOME'))
    spark_conf.set('spark.executorEnv.SPARK_HOME',
                   os.environ.get('SPARK_HOME'))
    spark_conf.set('spark.executorEnv.SPARK_EXTRA_CLASSPATH',
                   os.environ.get('SPARK_DIST_CLASSPATH'))

    # extra_class = spark_conf.get('spark.driver.extraClassPath') +\
    # ':/eos/project/s/swan/public/hadoop-mapreduce-client-core-2.6.0-cdh5.7.6.jar'  # noqa: E501
    # spark_conf.set('spark.driver.extraClassPath', extra_class)
    # spark_conf.set('spark.driver.extraClassPath','/eos/project/s/swan/public/hadoop-mapreduce-client-core-2.6.0-cdh5.7.6.jar')
    spark_conf.set('spark.driver.memory', '8g')

    print(functools.reduce(lambda x, y:
                           "%s\n%s\t%s" % (x, y[0], y[1]),
                           spark_conf.getAll(),
                           "Configuration data:"))

    sc = SparkContext(conf=spark_conf)
    spark = SparkSession(sc)
    return sc, spark, spark_conf


def stop_spark(sc, spark):
    """Manage stop of Spark context and session, if existing."""
    try:
        sc.stop()
    except Exception as ex:
        print("Cannot stop sc")
        print(ex)

    try:
        spark.stop()
    except Exception as ex:
        print("Cannot stop spark")
        print(ex)


def write_spark_df(df, out_path='', num_files=100):
    if out_path != '':
        df.coalesce(num_files).write.mode('overwrite').parquet(out_path)


def download_locally_as_csv(spark, hdfs_path,
                            csv_name="pandas_full",
                            csv_folder="datalake"):
    """Read a parquet file on HDFS and save a Pandas dataframe locally.

    spark: spark context
    hdfs_path: where your parquet files are located on the cluster
    csv_name: hthe name you want for your output pandas DataFrame in csv format
    csv_folder: the folder where you want to output your csv file ("datalake"
    by default)
    """
    print("Reading from HDFS... " + hdfs_path)
    df_on_hdfs = spark.read.parquet(hdfs_path + "/*")
    df_on_hdfs.cache()
    print("COLUMNS: %s" % df_on_hdfs.schema.names)
    if "plugin" in df_on_hdfs.schema.names:
        plugins_presents = df_on_hdfs.select('plugin').distinct().collect()
        print("Plugins in this DataFrame: %s" % plugins_presents)
    if "host" in df_on_hdfs.schema.names:
        different_hosts = df_on_hdfs.select('host').distinct().collect()
        nr_of_different_hosts = len(different_hosts)
        print("Nr of Different Hosts present: %s" % nr_of_different_hosts)
    print("Conversion to pandas DataFrame... (this may take a while)")
    pdf = df_on_hdfs.orderBy('n_min_group').toPandas()
    # create time series
    pdf["temporal_index"] = pd.to_datetime(pdf["event_timestamp"])
    pdf.set_index("temporal_index", inplace=True)
    pdf.drop(columns=['n_min_group'], inplace=True)
    # save csv locally
    filename = csv_name + ".csv"
    print("Saving ... %s in %s " % (filename, csv_folder))
    pdf.to_csv(csv_folder + "/" + filename)
    return pdf


# HDFS INSPECTION ------------------------------------------------------------


def get_list_dirs(inpath):
    """get list of subdirs from hdfs."""
    result = subprocess.run(['hdfs', 'dfs', '-ls', '-d', inpath],
                            stdout=subprocess.PIPE)
    lines = result.stdout.decode('utf-8')
    output = []
    for aline in lines.split('\n'):
        words = aline.split(' ')
        if len(words) > 6:
            output.append(words[-1])
    return output


def get_elements_in_dir(path, return_absolute_path=True,
                        do_not_modify_path=False):
    """Wrapper for the HDFS cmd 'hdfs dfs -ls -d {path}'.

    Get the list of elements in hdfs at the given path.
    e.g. if you want the document in folder 2019 pass:
    the command is expecting sth like:
    "/project/monitoring/collectd/cpu/2019"
    With return_absolute_path param you can decide if
    you want full absolute path or relative path
    (aka names of the subfolders).
    With do_not_modify_path = True you actually query the
    exact path you entered, with false a modification is made
    get always the subfolders (i.e. adding "/* at the end").
    """
    if not do_not_modify_path:
        if path[-1] != "/":
            path += "/"
        path += "*"
    cmd = 'hdfs dfs -ls -d {}'.format(path)
    # my_cmd_as_bytes = str.encode(cmd)
    try:
        raw_console_output = str(subprocess.check_output(cmd, shell=True))
    except subprocess.CalledProcessError:
        raw_console_output = ""
    # https://stackoverflow.com/questions/35750614/pyspark-get-list-of-files-directories-on-hdfs-path

    directories = re.findall(r"\/(.*?)\\n", raw_console_output)
    # https://stackoverflow.com/questions/2013124/regex-matching-up-to-the-first-occurrence-of-a-character
    # print(raw_console_output)
    if return_absolute_path:
        directories = ["/" + x for x in directories]
    else:
        directories = [x.split("/")[-1] for x in directories]
    return directories


def scan_plugin_folder(plugin_name,
                       base_collectd_path="/project/monitoring/collectd/"):
    """Collect all the folder paths in a plugin folder.

    It scans the plugin folder an return a list
    absolute paths sorted alphabetically. Each
    path with represent a month.
    The list will be sorted such that the first
    path is the one with the oldest data.
    """
    plugin_folder = base_collectd_path + plugin_name
    paths = []
    years_folders = get_elements_in_dir(plugin_folder,
                                        return_absolute_path=True)
    print("Year folders for this plugin: %s" % years_folders)
    # return the list only if this plugin is active in the 2020
    no_2020 = True
    for f in years_folders:
        if f.split("/")[-1] == "2020":
            no_2020 = False
    if no_2020:
        return []
    for year_folder in years_folders:
        # year_path = "/project/monitoring/collectd/"
        #              + plugin_name+"/"+year+"/*/*"
        # print("Inspecting: %s " % year_folder)
        months_folders = get_elements_in_dir(year_folder,
                                             return_absolute_path=True)
        # print("months_folders: %s" % months_folders)
        paths += months_folders
    return sorted(paths)


def get_date(a_string):
    """extract the date structure."""
    prog = re.compile("20[1-2][0-9]/[0-9][0-9]/[0-9][0-9]")
    return prog.search(a_string).group(0)


def loop_on_dates(get_spark_DF, inpath, out_base_path,
                  force_rewrite=False, debug=False):
    """Iterate on daily based data folders and extract/filter/transform data.

    Following the directory patterns of input data,
    that are organised by day, allow to run and extract data
    day by day, and store the results in daily smaller files
    in local area.
    Avoids to run again on daily datasets already processed,
    except if force_rewrite is True.

    Arguments:
    get_spark_DF: function that specifies the ETL
    inpath: list of input paths (list)
    out_base_path: directory where to store the extracted daily based data
    force_rewrite: overwrite the already processed days, and rerun on the whole
                  input dataset
    debug: verbosity
    """
    dir_list = get_list_dirs(inpath)
    out_dir_list = get_list_dirs("%s/*" % out_base_path)
    if force_rewrite:
        out_dir_list = []

    if debug:
        for v in out_dir_list:
            print('Existing out dirs %s' % v)

    date_list = defaultdict(list)
    for x in dir_list:
        date_list[get_date(x)].append(x)

    """loop on dates"""
    for adate, somedirs in date_list.items():
        if debug:
            print("---------- checking date ", adate)
        outpath = "%s/%s" % (out_base_path, adate.replace("/", "_"))
        # the tmp data go in another dir
        outpathtmp = "%s/tmp_%s" % (out_base_path, adate.replace("/", "_"))

        # removing and refreshing the tmp
        # given that the tmp is going to evolve, running on tmp in any case
        if outpathtmp in out_dir_list:
            if debug:
                print("\tremoving %s " % outpathtmp)
            subprocess.run(['hdfs', 'dfs', '-rm',
                            '-r', '-skipTrash',
                            outpathtmp], stdout=subprocess.PIPE)
        is_tmp = False
        if somedirs[0].find(".tmp") > 0:
            outpath = outpathtmp
            is_tmp = True

        if outpath not in out_dir_list or is_tmp:
            if debug:
                print("\"---------- the output dir is ", outpath)
                print("\"---------- the input dirs are %s" % somedirs)
            try:
                get_spark_DF(somedirs, outpath)
            except Exception as ex:
                print("problem?!")
                print(ex)


# JSON SPECIFIC FUNCTIONS ----------------------------------------------------


def get_schema(spark, in_path, json_schema):
    """Autodiscover schema from file inspections.

    In order to generate a schema of the data,
    use this utility with a subset of the data
    (1 day for instance)
    spark: spark session
    inpath: input path of data to discover the schema
    jsonschema: output file where to store the schema in json format
    """
    full_df = spark.read.json(in_path)
    file = open(json_schema, 'w')
    file.write(json.dumps(full_df.schema.json()))
    file.close()
    return full_df


def load_schema(json_schema):
    """Load schema from json file."""
    nschema = json.loads(open(json_schema, 'r').read())
    logschema = StructType.fromJson(json.loads(nschema))
    return logschema
