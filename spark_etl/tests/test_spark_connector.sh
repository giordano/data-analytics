#!/bin/bash
# This script should run in an environment (eg: docker container) configured
# to run jupyter notebooks and pyspark and connect to Spark 
# E.g.
# export CVMFSDIR=/tmp/giordano/cvmfs_hep/noCI-20200326-124623-388198334
# export CI_USER=bmkwg
# read -s CI_USER_PASSWD
# docker run -it --rm -e KRBUSER=$CI_USER -e KRBPASSWD=$CI_USER_PASSWD -v $CVMFSDIR:/cvmfs:shared -v /tmp:/tmp  -v `pwd`:/work --net host gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/sparknotebook:latest

function fail(){
  echo -e "\n------------------------\nFailing '$@'\n------------------------\n" >&2
  echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo -e "\n$0 finished (NOT OK) at $(date)\n"
  echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
  exit 1
}

WORK_DIR=$(readlink -f $(dirname $0))
echo WORK_DIR $WORK_DIR

[[ -z "$KRB5CCNAME" ]] && KRB5CCNAME=/tmp/krb5cc_docker
export KRB5CCNAME
echo -e "\nKRB5CCNAME file $KRB5CCNAME"

[[ -z "$KRBUSER" ]] && KBUSER=$USER
echo "Kerberos user $KRBUSER"

if [[ -z "$KRBPASSWD" ]];
then
    echo "Enter password: (it won't be displayed)"
    read -s KRBPASSWD
fi

echo -e "\nRunning kinit...\n"
(echo $KRBPASSWD | kinit -c $KRB5CCNAME $KRBUSER@CERN.CH) || fail 'kinit'

klist -c $KRB5CCNAME


#echo -e "\nInstalling data-analytics\n"
#pip install --user /work/data-analytics || fail 'installation'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\nSetting hadoop libs\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

export LCG_VIEW=/cvmfs/sft.cern.ch/lcg/views/LCG_96python3/x86_64-centos7-gcc8-opt
source $LCG_VIEW/setup.sh  || fail 'Setting hadoop'
source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh analytix
export PYTHONPATH=/usr/local/lib/python3.6/site-packages/:$PYTHONPATH

echo PYTHONPATH $PYTHONPATH
echo LD_LIBRARY_PATH $LD_LIBRARY_PATH
echo JAVA_HOME $JAVA_HOME
echo SPARK_HOME $SPARK_HOME
echo SPARK_DIST_CLASSPATH $SPARK_DIST_CLASSPATH

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest access to hdfs\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

hdfs dfs -ls /project/monitoring || fail 'test access to hdfs'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest python pyspark module\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
python3 ${WORK_DIR}/test_spark_connector.py

# This still does not work in the swan image (issue with nbconvert libraries)
#echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
#echo -e "\ntest jupyter notebook\n" 
#echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
#jupyter nbconvert -to notebook --execute ${WORK_DIR}/test_notebook_spark_connector.ipynb